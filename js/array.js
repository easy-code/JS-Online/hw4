// Массивы. Задачи на методы

// --------------------------------------------------------------------------------------------------------------
// 1. Создать функцию, которая принимает массив,
//    а возвращает новый массив с дублированными элементами входного массива:
//    doubleArray([1, 2, 3]) = [1, 2, 3, 1, 2, 3]

function doubleArray(arr) {
    let copy = arr.slice();

    for(let i = 0; i < arr.length;  i++ ) {
        copy.push(arr[i]);
    }

    return copy;
}
console.log(doubleArray([1, 2, 3]));

// --------------------------------------------------------------------------------------------------------------
// 2. Получить последний элемент массива (массив не менять). Использовать функцию
// --------------------------------------------------------------------------------------------------------------
let lastElementOfArray = arr => arr[arr.length - 1];
console.log(lastElementOfArray([1, 2, 3]));

// --------------------------------------------------------------------------------------------------------------
// 3. Создать функцию, которая принимает число N и возвращает массив, заполненный числами от 1 до N:
//    getArray(10); // [1,2,3,4,5,6,7,8,9,10]
// --------------------------------------------------------------------------------------------------------------
function getArray(n) {
    let array = [];

    for (let i = 1; i <= n; i++ ){
        array.push(i);
    }

    return array;
}
console.log(getArray(10));

// --------------------------------------------------------------------------------------------------------------
// 4. Создать функцию, которая принимает произвольное (любое) число массивов и удаляет из каждого массива
//    первый элемент, а возвращает массив из отсавшихся значений:
//    changeCollection([1, 2, 3], ['a', 'b', 'c']) -> [[2,3], ['b', 'c']],
//    changeCollection([1, 2, 3]) -> [[2,3]] и т.д.
// --------------------------------------------------------------------------------------------------------------
function changeCollection(...arr) {
    for (let i = 0; i < arr.length; i++ ) {
        arr[i].shift();
    }

    return arr;
}
console.log(changeCollection([1, 2, 3], ['a', 'b', 'c']));
console.log(changeCollection([1,2,3]));


// Массивы. Задачи на методы

// --------------------------------------------------------------------------------------------------------------
// 1. Дана произвольная строка "bcdaeflmjgkhi" - упорядочить буквы по алфавиту и
//    вернуть строку с буквами в обратном порядке ("mlkjihgfedcba"). Оформить в виде функции.
// --------------------------------------------------------------------------------------------------------------
let sortAndRevertString = someString => someString.split('').sort().reverse().join('');

console.log(sortAndRevertString('bcdaeflmjgkhi')); // mlkjihgfedcba

// --------------------------------------------------------------------------------------------------------------
// 2. Отсортировать массив [2, 4, 7, 1, -2, 10, -9] в обратном порядке: [10, 7, 4, 2, 1, -2, -9].
//    Используйте функцию
// --------------------------------------------------------------------------------------------------------------
let sortAndRevertArray = arr => arr.sort((num1, num2) => num2 - num1);
console.log(sortAndRevertArray([2, 4, 7, 1, -2, 10, -9])); // [10, 7, 4, 2, 1, -2, -9]

// --------------------------------------------------------------------------------------------------------------
// 3. Написать функцию, которая принимает три аргумента: произвольный массив и два числа,
//    первое из которых означает начальный номер элемента в массива, второе - конечный номер.
//    Функция должна вернуть новый массив, состоящий из элементов первой коллекции согласно аргументам (с -по):
//    getNewArray(['a', 'b', 'c', 'd', 'e', 'f'], 2, 4) = ['c', 'd', 'e']
//    Исходный массив не менять. Циклы не использовать.
// --------------------------------------------------------------------------------------------------------------
let getNewArray = (arr, start, end) => arr.slice(start, end + 1);
console.log(getNewArray(['a', 'b', 'c', 'd', 'e', 'f'], 2, 4)); // ["c", "d", "e"]

// --------------------------------------------------------------------------------------------------------------
// 4. Удвоить все элементы массива, не используя циклы
//    ['one', 2, 'three', 4] -> ['one', 2, 'three', 4, 'one', 2, 'three', 4]
// --------------------------------------------------------------------------------------------------------------
let double = arr => arr.concat(arr);
console.log(double(['one', 2, 'three', 4])); // ["one", 2, "three", 4, "one", 2, "three", 4]

// --------------------------------------------------------------------------------------------------------------
// 5. Удалить из [1, 2, 3, 4, 5] второй и третий элементы (3, 4)
// --------------------------------------------------------------------------------------------------------------
function spliceArr(arr) {
    arr.splice(2, 2);

    return arr;
}
console.log(spliceArr([1, 2, 3, 4, 5])); // [1, 2, 5]

// --------------------------------------------------------------------------------------------------------------
// 6. Удалить из [1, 2, 3, 4, 5] второй и третий элементы (3, 4) и на их место вставить 'three', 'four'
// --------------------------------------------------------------------------------------------------------------
function spliceAndAdd(arr) {
    arr.splice(2, 2, 'three', 'four');

    return arr;
}
console.log(spliceAndAdd([1, 2, 3, 4, 5])); //  [1, 2, "three", "four", 5]

// --------------------------------------------------------------------------------------------------------------
// 7. Вставить в произвольный массив после третьего элемента любое значение, например:
//    ['I', 'am', 'an', 'array'] -> ['I', 'am', 'an', 'awesome', 'array']
// --------------------------------------------------------------------------------------------------------------
function addAfterThird(arr, el) {
    arr.splice(3, 0, el);

    return arr;
}
console.log(addAfterThird(['I', 'am', 'an', 'array'], 'awesome')); //   ["I", "am", "an", "awesome", "array"]


// --------------------------------------------------------------------------------------------------------------
// 8. Отсортируйте массив массивов так, чтобы вначале распологалоись наименьшие массивы
//    (размер массива определяется его длиной):
//    [[14, 15], [1], ['a', 'c', 'd']] -> [[1], [14, 15], ['a', 'c', 'd']]
// --------------------------------------------------------------------------------------------------------------
let sortArray = arr => arr.sort((prev, next) => prev.length - next.length);
console.log(sortArray([[14, 15], [1], ['a', 'c', 'd']])); // [[1], [14, 15], ['a', 'c', 'd']]

// --------------------------------------------------------------------------------------------------------------
// 9. Создать копию произвольного массива (slice, concat)
// --------------------------------------------------------------------------------------------------------------
let sliceArray = arr => arr.slice(0);
let concatArray = arr => [].concat(arr);

console.log(concatArray([[14, 15], [1], ['a', 'c', 'd']]));
console.log(sliceArray([[14, 15], [1], ['a', 'c', 'd']]));

// --------------------------------------------------------------------------------------------------------------
// 10. Есть массив объектов:
//     [
//       {cpu: 'intel', info: {cores: 2, cache: 3}},
//       {cpu: 'intel', info: {cores: 4, cache: 4}},
//       {cpu: 'amd', info: {cores: 1, cache: 1}},
//       {cpu: 'intel', info: {cores: 3, cache: 2}},
//       {cpu: 'amd', info: {cores: 4, cache: 2}}
//     ]
//     Отсортировать их по возрастающему количеству ядер (cores).
// --------------------------------------------------------------------------------------------------------------
let objArr = [
    {cpu: 'intel', info: {cores: 2, cache: 3}},
    {cpu: 'intel', info: {cores: 4, cache: 4}},
    {cpu: 'amd', info: {cores: 1, cache: 1}},
    {cpu: 'intel', info: {cores: 3, cache: 2}},
    {cpu: 'amd', info: {cores: 4, cache: 2}}
];

let sortCores = arr => arr.sort((prev, next) => prev.info.cores - next.info.cores);
console.log(sortCores(objArr));

// --------------------------------------------------------------------------------------------------------------
// 11. Создать функцию, которая будет принимать массив продуктов и две цены.
//     Функция должна вернуть все продукты, цена которых находится в указанном диапазоне,
//     и отсортировать от дешевых к дорогим:
//
//     const products = [
//       {title: 'prod1', price: 5.2},
//       {title: 'prod2', price: 0.18},
//       {title: 'prod3', price: 15},
//       {title: 'prod4', price: 25},
//       {title: 'prod5', price: 18.9},
//       {title: 'prod6', price: 8},
//       {title: 'prod7', price: 19},
//       {title: 'prod8', price: 63}
//     ]
//
//     filterCollection(products, 15, 30) -> [{
//       {title: 'prod3', price: 15},
//       {title: 'prod5', price: 18.9},
//       {title: 'prod7', price: 19},
//       {title: 'prod4', price: 25}
//     }]
// --------------------------------------------------------------------------------------------------------------
const products = [
    {title: 'prod1', price: 5.2},
    {title: 'prod2', price: 0.18},
    {title: 'prod3', price: 15},
    {title: 'prod4', price: 25},
    {title: 'prod5', price: 18.9},
    {title: 'prod6', price: 8},
    {title: 'prod7', price: 19},
    {title: 'prod8', price: 63}
];

function filterCollection(arr, start, end) {
    for (let i = 0; i < arr.length; i++) {
        let value = arr[i].price;

        if (value < start || value > end) {
            arr.splice(i--, 1);
        }
    }

    return arr.sort((prev, next) => prev.price - next.price);
}

console.log(filterCollection(products, 15, 30));
