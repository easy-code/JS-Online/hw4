// DOM

// --------------------------------------------------------------------------------------------------------------
// 1. Создать функцию, которая принимает два элемента.
//    Функция проверяет, является ли первый элемент родителем для второго:
//    isParent(parent, child);
//
//    isParent(document.body.children[0], document.querySelector('mark'));
//    // true так как первый див является родительским элемнтом для mark
//
//    isParent(document.querySelector('ul'), document.querySelector('mark'));
//    // false так ul НЕ является родительским элементом для mark
// --------------------------------------------------------------------------------------------------------------

/*
const isParent = (parent, child) => {
    let parentElement = child.parentElement;

    while (parentElement !== document) {
        if (parentElement === parent) return true;

        parentElement = parentElement.parentElement;
    }
    return false;
};
*/

let isParent = (parent, child) => (child === parent) ? false : parent.contains(child);

console.log(isParent(document.body.children[0], document.querySelector('mark'))); // true
console.log(isParent(document.querySelector('ul'), document.querySelector('mark'))); // false

// --------------------------------------------------------------------------------------------------------------
// 2. Получить список всех ссылок, которые не находятся внутри списка ul
// --------------------------------------------------------------------------------------------------------------
let aList = document.querySelector('ul').getElementsByTagName('a');

let ul = document.querySelector('ul');
let a = document.getElementsByTagName('a');

for (let i = 0; i < a.length; i++) {
    if (!isParent(ul, a[i])) {
        console.log(a[i]);
    }
}

console.log(aList);

// --------------------------------------------------------------------------------------------------------------
// 3. Найти элемент, который находится перед и после списка ul
// --------------------------------------------------------------------------------------------------------------
console.log(ul.previousElementSibling); // div
console.log(ul.nextElementSibling); // span

// --------------------------------------------------------------------------------------------------------------
// 4. Посчитать количество элементов li в списке
// --------------------------------------------------------------------------------------------------------------
console.log(ul.children.length); // 4

// --------------------------------------------------------------------------------------------------------------
// 5. Создать функцию которая будет выводить сообщение на страницу.
//    Функция принимает текст сообщения и на странице показывает блок с этим сообщением.
//    За разметку взять alert alert-info из бутстрапа 4.
//    Элемент должен быть сразу на странице над списком с задачами и через стили ему должно быть задано
//    display:none свойство. При вызове вашей функции в скрипте добавляется класс show на элемент,
//    на данный класс в стилях должно быть прописано display: block. Для того что бы добавить класс можно
//    использовать следующий метод element.classList.add('имя класса').
//    Также можно добавить что бы сообщение исчезало через определенное врямя,
//    для этого нужно добавить setTimeout который принимает функцию и время через которое эта функция
//    должна сработать, внутри функции вы можете снять класс с alert сделать это можно методом
//    element.classList.remove('имя класса').
// --------------------------------------------------------------------------------------------------------------
let div = document.createElement('div');
div.className = "alert alert-info";

function showMessage(text) {
    document.body.insertBefore(div, document.body.firstChild);
    div.classList.add('d-block');
    div.innerHTML = text;

    setTimeout(() => div.classList.remove('show'), 2000);
}

// showMessage('some text');
