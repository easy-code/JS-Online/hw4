## Email after first lesson

##### Cсылка на видео
[Video]()

##### Ссылки на презентацию
[First](https://docs.google.com/presentation/d/112LXuLBXmyZPgr2IZMWLhOEuB20YVcOnhPD3WaVmPFk/pub?start=false&loop=false&delayms=3000&slide=id.gf9a5aadb0_0_57)

[Second](https://docs.google.com/presentation/d/1PcXmIEMF0It5fNrNoY6GUHWv6uPUmWkh8UJbp617JDw/pub?start=false&loop=false&delayms=3000&slide=id.p)

##### Справочники
[developer.mozilla.org](https://developer.mozilla.org/uk/docs/Web/JavaScript)

[learn.javascript.ru](https://learn.javascript.ru/getting-started)

##### Стиль кода
https://learn.javascript.ru/coding-style

##### Ссылки из презентации
[Array_1](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array)
[Array_2](https://learn.javascript.ru/array)

[Sort](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/sort)

[Быстрая сортировка](https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0)

[Окружение: DOM, BOM и JS](https://learn.javascript.ru/browser-environment)

[BOM](https://maxello.gitbooks.io/js-note/content/10_dom/bom-obekti_navigator,_screen,_location,_frames.html)

[DOM](https://learn.javascript.ru/dom-nodes)

[CSSOM](https://habrahabr.ru/post/320430/)

[Пример](https://plnkr.co/edit/HgahYSS9TsiNnip8usJe?p=preview)

[area](https://docs.google.com/presentation/d/1PcXmIEMF0It5fNrNoY6GUHWv6uPUmWkh8UJbp617JDw/pub?start=false&loop=false&delayms=3000&slide=id.g10bf196d01_0_247)

[area example](https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_areamap)

[Пример коллекции](https://codepen.io/iliatcymbal/pen/GZNZva?editors=0010)